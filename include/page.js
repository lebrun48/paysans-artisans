//Page

function menuListener()
{
  //toolTip
}

//Cash table

function cashListener()
{
  $("#cashMove").click(function () {
    userRequest({xAction: "edit", action: "insert", page: "cash", type: 4});
  });

  $("#cashContent").click(function () {
    userRequest({xAction: "edit", page: "cash", action: "insert", type: 5});
  });

  $("#cashInit").click(function () {
    axExecute({xAction: "submit", action: "insert", page: "cash", type: 7});
  });

  $("#cashLogout").click(function () {
    axExecute("logout");
  });

  numeral.locale('fr');
  numeral.defaultFormat("0.00$");
  tableListener("cash");
}

function cashSumaryUpdate($inp)
{
  $("#printContent").hide();
  if ($inp.attr("name") != "val_bank") {
    amount = $inp.val() * $inp.parent().prev().data("val");
    amount = amount ? numeral(amount).format() : '';
    $inp.parent().next().text(amount);
  }

  sum = 0;
  $(".inp-cash[value!=0]").each(function () {
    sum += $(this).val() * (this.name == 'val_bank' ? 1 : $(this).parent().prev().data("val"));
  });

  cur = $("#contentCurrent").data("content");
  $("#contentCash").text(numeral(sum).format());
  $("#contentDiff").html("<span class=money-" + ((diff = sum - cur) < 0 ? "neg" : "pos") + ">" + numeral(diff).format() + "</span>");
  $("[name='val_move']").val(diff.toFixed(2));

}

function ticketFormListener()
{
  $("#ed_productNb").change(function () {
    axExecute({xAction: 'getProduct', nb: this.value, lastPrice: $("[name='type']").val() == 1 ? 1 : 0});
  }).keyup(function (event) {
    switch (c = event.key) {
      case '.':
        break;

      case ',':
      case ' ':
      case '-':
      case ';':
        this.value = this.value.replace(c, '.');
        break;

      default:
        (c < '0' || c > '9') && (this.value = this.value.replace(c, ''));
        break;
    }
  });
}

function contentFormListener()
{
  $(".inp-cash").keyup(function () {
    cashSumaryUpdate($(this));
  });
  $(".inp-cash").change(function () {
    cashSumaryUpdate($(this));
  });
}

function contentFormValidate()
{
  ret = false;
  $(".inp-cash").each(function() {
    return ret = this.reportValidity();
  });
  return ret;
}

function toggleViewAll(checked, id)
{
  $("#" + id + " tr[class*='otherday']").toggle(checked)
}


//Ticket table

function computeCaution()
{
  nbr = $("#ed_number").val();
  unitary = $("#ed_unitary").val();
  if (!unitary || !nbr) {
    val = 0;
  } else {
    val = nbr * unitary;
  }
  $("#sumCaution").html("<small>Montant:</small> " + val.toFixed(2) + "€");
}

function ticketListener()
{
  tableListener("ticket");

  $("#ticketSale").click(function () {
    userRequest({xAction: "edit", action: "insert", page: "ticket", type: 1});
  });

  $("#ticketReturn").click(function () {
    userRequest({xAction: "edit", action: "insert", page: "ticket", type: 2});
  });

  $("#ticketCaution").click(function () {
    userRequest({xAction: "edit", action: "insert", page: "ticket", type: 3});
  });

  $("#ticketCancel").click(function () {
    axExecute("cancel", {page: "ticket"});
  });

  $("#clientReceived").keyup(function () {
    returnClient(this, $(this).data("sum"));
  });

  $("#clientReceived").change(function () {
    returnClient(this, $(this).data("sum"));
  });

}

function returnClient(obj, sum)
{
  if (!sum) {
    sum = 0;
  }
  if ((ret = sum - obj.value) < 0) {
    $("#toReturnOrReceive").toggleClass("money-neg", true).toggleClass("money-pos", false).text("A rendre");
    $("#clientReturn").toggleClass("money-neg", true).toggleClass("money-pos", false).text(Math.abs(ret).toFixed(2).replace(".", ",") + '€');
  } else {
    $("#toReturnOrReceive").toggleClass("money-neg", false).toggleClass("money-pos", true).text("A recevoir");
    $("#clientReturn").toggleClass("money-neg", false).toggleClass("money-pos", true).text(Math.abs(ret).toFixed(2).replace(".", ",") + '€');
  }
}

//admin page
function adminListener()
{
  usersListener();
  prListener();
}


//users tableid
function usersListener()
{
  tableListener("users");
}

//pr tableid
function prListener()
{
  tableListener('pr');

  $("[name='cash-sheet'").change(function () {
    window.open("cashPrint.php?current=" + $(this).val() + '&pr=' + $(this).parents("tr").first().data("primary")[0]);
    this.selectedIndex = 0;
  });

  $("[id='lastClosure'").click(function () {
    window.open("cashPrint.php?current=" + $(this).data("value") + '&pr=' + $(this).parents("tr").first().data("primary")[0]);
    this.selectedIndex = 0;
  });

}

function updateProduct(toUpdate)
{
  for (i in toUpdate) {
    i == "ed_tva" && $("#" + i + toUpdate[i]).prop("checked", true) || $("#" + i).val(toUpdate[i]).focus();
  }
  toUpdate.ed_product && $("#ed_number").focus() || $("#ed_productNb").focus().select();
}