<?php

include "site/common.php";
include_once "pages/cashOp/DisplayLine.php";
Plugin::registerAutoLoad("fpdf");

define('FPDF_FONTPATH', 'fpdf/font');

function decodeWin($val)
{
  return mb_convert_encoding($val, "Windows-1252");
}

function printCash()
{
  if ($_REQUEST["pr"]) {
    dbUtil()->getTable("cash")[DB_DEFAULT_WHERE] = "pr=" . $_REQUEST["pr"] . " and not isTicket";
    $prName = decodeWin(strtoupper(dbUtil()->result(dbUtil()->selectRow("pr", "name", "ri=" . $_REQUEST["pr"]), 0)));
  }
  else {
    $prName = decodeWin(strtoupper(utils()->userSession()["prName"]));
  }
  $res = dbUtil()->selectRow("cash", "ri, move, date", "type=" . CASH_TYPE_INIT . (($current = $_REQUEST["current"]) ? " and ri<$current" : '') . " order by ri desc limit 1");
  $move = 0;
  $start = $current ? "ri<$current" : '1';
  if (dbUtil()->num_rows($res)) {
    $tup = dbUtil()->fetch_row($res);
    $start .= " and ri>$tup[0]";
    $init = $tup[1];
  }
  $begin = dbUtil()->fetch_row(dbUtil()->selectRow("cash", "type, description", "$start order by ri desc limit 1", 1));
  if ($begin[0] != CASH_TYPE_ADJUST || !$begin[1] || !($begin = json_decode($begin[1], true)["activityDate"])) {
    if (!$current) {
      return;
    }
    ($begin = dbUtil()->result(dbUtil()->selectRow("cash", "date", "ri=$current", false), 0)) && ($begin = DateTime::createFromFormat("Y-m-d", $begin)->format("d/m/Y"));
  }


  $res = dbUtil()->selectRow("cash", "move, description", "$start and type=" . CASH_TYPE_TRANSFER);
  while ($tup = dbUtil()->fetch_row($res)) {
    switch ($tup[1]) {
      case 1:
        $banque += $tup[0];
        break;

      case 2:
        $banque -= $tup[0];
        break;

      case 3:
        $bar += $tup[0];
        break;

      case 4:
        $bar -= $tup[0];
    }
  }

//build header
  $pdf = new FPDF();

  $pdf->AddPage();
  $pdf->SetMargins(21, 25);
  $pdf->SetFont('Arial', '', 18);

  $pdf->Write(10, "PAYSAN-ARTISANS SCRL - FS\n");
  $pdf->SetFontSize(10);
  $pdf->Write(6, "POINT DE R'ALIMENT DE ");
  $pdf->SetX($pdf->GetX() + 47);
  $pdf->Write(6, "$prName\n");
  $pdf->SetFontSize(8);



  $pdf->Cell(135, 5, decodeWin("Situation de la cloture de la semaine précédente"), 1);
  $pdf->Cell(30, 5, utils()->getMoney($init, false), 1, 1, 'C');

  $pdf->Cell(135, 5, decodeWin("mvt banque(+ de la banque PA vers la caisse PA, - de la caisse PA vers la banque PA)"), 1);
  $pdf->Cell(30, 5, utils()->getMoney($banque, false, true), 1, 1, 'C');

  $pdf->SetTextColor(200, 20, 20);
  $pdf->Cell(135, 5, decodeWin("transfert BAR du PR (+ du BAR vers la caisse PA, - de la caisse PA vers le BAR)"), 1);
  $pdf->Cell(30, 5, utils()->getMoney($bar, false, true), 1, 1, 'C');
  $pdf->SetTextColor(0);

  $init += $banque + $bar;
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->Cell(135, 8, decodeWin("Stituation initiale en date du $begin"), 1);
  $pdf->SetTextColor(200, 20, 20);
  $pdf->Cell(30, 8, utils()->getMoney($init, false, true), 1, 1, 'C');


  global $sumIn, $sumOut;
  $res = dbUtil()->selectRow("cash", "*", "$start and (type=" . CASH_TYPE_RETURN . " or type=" . CASH_TYPE_SALE . ") and tva=1 order by ri asc");
  InOut($pdf, $res, 6);
  $totIn += $sumIn;
  $totOut += $sumOut;

  $res = dbUtil()->selectRow("cash", "*", "$start and (type=" . CASH_TYPE_RETURN . " or type=" . CASH_TYPE_SALE . ") and tva=2 order by ri asc");
  InOut($pdf, $res, 21);
  $totIn += $sumIn;
  $totOut += $sumOut;

  $sumIn = $sumOut = 0;
  $pdf->SetTextColor(0);
  $pdf->SetFont('Arial', '', 9);
  $tva = 0;
  $pdf->Cell(165, 9, decodeWin("$tva% $tva% $tva% $tva% CONSIGNES $tva% $tva% $tva% $tva% "), 1, 1, 'C');

  $sumOut = 0;
  $pdf->SetFontSize(7);
  $pdf->cell(75, 4.5, decodeWin("Libellé"), 1, 0, 'C');
  $pdf->cell(10, 4.5, decodeWin("Nbre"), 1, 0, 'C');
  $pdf->cell(10, 4.5, decodeWin("Prix"), 1, 0, 'C');
  $pdf->Cell(40, 4.5, decodeWin("Remarque"), 1, 0, 'C');
  $pdf->Cell(15, 4.5, decodeWin("Entrées"), 1, 0, 'C');
  $pdf->Cell(15, 4.5, decodeWin("Sorties"), 1, 1, 'C');

  $res = dbUtil()->selectRow("cash", "sum(number), sum(number*unitary)", "$start and type=" . CASH_TYPE_CAUTION . " group by unitary");
  while ($tup = dbUtil()->fetch_row($res)) {
    $pdf->cell(75, 4.5, '', 1, 0, 'L');
    $pdf->cell(10, 4.5, decodeWin($tup[0]), 1, 0, 'R');
    $pdf->cell(10, 4.5, decodeWin(utils()->getMoney(abs($tup[1]) / $tup[0])), 1, 0, 'R');
    $pdf->Cell(40, 4.5, decodeWin(""), 1, 0, 'C');
    if ($tup[1] < 0) {
      $pdf->Cell(15, 4.5, decodeWin(""), 1, 0, 'C');
      $pdf->Cell(15, 4.5, decodeWin(utils()->getMoney(-$tup[1], false)), 1, 1, 'R');
    }
    else {
      $pdf->Cell(15, 4.5, decodeWin(utils()->getMoney($tup[1], false)), 1, 0, 'C');
      $pdf->Cell(15, 4.5, decodeWin(""), 1, 1, 'R');
    }
    if ($tup[1] < 0) {
      $sumOut += abs($tup[1]);
    }
    else {
      $sumIn += $tup[1];
    }
  }

  $pdf->Cell(165, 4.5, '', 1, 1);
  $pdf->Cell(135, 4.5, decodeWin("S-S totaux à $tva%"), 1, 0, 'C');
  $pdf->SetTextColor(200, 20, 20);
  $pdf->Cell(15, 4.5, utils()->getMoney($sumIn, false), 1, 0, 'R');
  $pdf->Cell(15, 4.5, utils()->getMoney($sumOut, false), 1, 1, 'R');

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('Arial', 'B', 9);
  $pdf->Cell(135, 6, decodeWin("Total Entrées / Sorties à $tva%"), 1);
  $pdf->SetTextColor(200, 20, 20);
  $pdf->Cell(30, 6, utils()->getMoney($sumIn - $sumOut, false), 1, 1, 'C');
  $totOut += $sumOut;
  $totIn += $sumIn;

  if ($rounds = dbUtil()->result(dbUtil()->selectRow("cash", "sum(move)", "$start and type=" . CASH_TYPE_ROUNDED), 0)) {
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(135, 6, decodeWin("Total des arrondis à 5 cents "), 1);
    $pdf->SetTextColor(200, 20, 20);
    $pdf->Cell(30, 6, utils()->getMoney($rounds, false), 1, 1, 'C');
  }
  if ($adjust = round(dbUtil()->result(dbUtil()->selectRow("cash", "sum(move)", "$start and type=" . CASH_TYPE_ADJUST), 0), 2)) {
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Cell(135, 6, decodeWin("Différence de caisse "), 1);
    $pdf->SetTextColor(200, 20, 20);
    $pdf->Cell(30, 6, utils()->getMoney($adjust, false), 1, 1, 'C');
  }
  $pdf->Cell(165, 9, '', 1, 1);


  $init += $totIn - $totOut + $rounds + $adjust;
  $pdf->SetFont('Arial', 'B', 12);
  $pdf->SetTextColor(0);
  $pdf->Cell(135, 8, decodeWin("Total final caisse en date du $begin"), 1);
  $pdf->SetTextColor(200, 20, 20);
  $pdf->Cell(30, 8, utils()->getMoney($init, false), 1, 1, 'C');

  $pdf->Output('I');
}

function InOut($pdf, $res, $tva)
{
  global $sumIn, $sumOut;
  $sumIn = $sumOut = 0;
  $pdf->SetTextColor(0);
  $pdf->SetFont('Arial', '', 9);
  $pdf->Cell(165, 9, decodeWin("$tva% $tva% $tva% $tva% " . ($tva == 6 ? "LA PLUPART DES PRODUITS" : "ALCOOLS (bières, vins, rhum, verre, ...)") . " $tva% $tva% $tva% $tva% "), 1, 1, 'C');

  $pdf->SetFontSize(7);
  $pdf->Cell(15.5, 4.5, decodeWin("N° produit"), 1, 0, 'C');
  $pdf->Cell(56, 4.5, decodeWin("libellé"), 1, 0, 'C');
  $pdf->Cell(63.5, 4.5, decodeWin("remarque(remboursé car manquant, abîmé ...)"), 1, 0, 'C');
  $pdf->Cell(15, 4.5, decodeWin("Entrées"), 1, 0, 'C');
  $pdf->Cell(15, 4.5, decodeWin("Sorties"), 1, 1, 'C');
  while ($tup = dbUtil()->fetch_assoc($res)) {
    $pdf->Cell(15.5, 4.5, decodeWin($tup["productNb"]), 1, 0, 'L');
    if (!$tup["move"]) {
      $tup["product"] .= (($tmp = $tup["number"]) > 1 ? ". $tmp x " . utils()->getMoney($tup["unitary"]) : '');
      $tup["move"] = $tup["number"] * $tup["unitary"];
    }
    $pdf->Cell(56, 4.5, decodeWin($tup["product"]), 1, 0, 'L');
    $cont = DisplayLine::$inOutReason[$tup["description"][0]] . substr($tup["description"], 1) . (($tmp = $tup["discount"]) ? " (remise $tmp% de " . utils()->getMoney($tup["move"]) . ")" : '');
    $pdf->Cell(63.5, 4.5, decodeWin($cont), 1, 0, 'C');
    $cont = $tup["move"];
    if ($tmp) {
      $cont = round($cont * ($tup["type"] == CASH_TYPE_SALE ? 1 - $tmp / 100 : $tmp / 100), 2);
    }
    $out = utils()->getMoney($cont, false);
    if ($tup["type"] == CASH_TYPE_RETURN) {
      $sumOut += $cont;
      $pdf->Cell(15, 4.5, '', 1, 0, 'R');
      $pdf->Cell(15, 4.5, decodeWin($out), 1, 1, 'R');
    }
    else {
      $sumIn += $cont;
      $pdf->Cell(15, 4.5, decodeWin($out), 1, 0, 'R');
      $pdf->Cell(15, 4.5, '', 1, 1, 'R');
    }
  }
  $pdf->Cell(165, 4.5, '', 1, 1);
  $pdf->Cell(15.5, 4.5, "", 1, 0, 'C');
  $pdf->Cell(56, 4.5, decodeWin("S-S totaux à $tva%"), 1, 0, 'L');
  $pdf->Cell(63.5, 4.5, "", 1, 0, 'C');
  $pdf->SetTextColor(200, 20, 20);
  $pdf->Cell(15, 4.5, utils()->getMoney($sumIn, false), 1, 0, 'R');
  $pdf->Cell(15, 4.5, utils()->getMoney($sumOut, false), 1, 1, 'R');

  $pdf->SetTextColor(0, 0, 0);
  $pdf->SetFont('Arial', 'B', 9);
  $pdf->Cell(135, 6, decodeWin("Total Entrées / Sorties à $tva%"), 1);
  $pdf->SetTextColor(200, 20, 20);
  $pdf->Cell(30, 6, utils()->getMoney($sumIn - $sumOut, false), 1, 1, 'C');
  $pdf->Cell(165, 9, '', 1, 1);
}

function printContent()
{
  $pdf = new FPDF;
  $pdf->AddPage();
  $pdf->SetMargins(50, 25);
  $pdf->SetFont('Arial', '', 18);

  $tup = dbUtil()->fetch_row(dbUtil()->query("select type, description, move, date from " . dbUtil()->getTableName("cash") . " where ri=" . $_REQUEST["content"]));
  if ($tup[0] != CASH_TYPE_ADJUST) {
    throw new Exception("Invalid tuple cash content" . debugLog($tup, "tuple", DEBUGLOG_GET_DUMP));
  }

  $content = json_decode($tup[1], true);
  $values = array(/* 500, 200, 100, */3 => 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05);
  $pdf->Write(10, "\nContenu caisse\n");
  $pdf->Write(10, (($tmp = $content["activityDate"]) ? decodeWin("Activité du $tmp") : "Contenu au " . DateTime::createFromFormat("Y-m-d", $tup[3])->format("d/m/Y")) . "\n\n");
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->Cell(50, 10, "EN BANQUE", 0, 0, "C");
  ($sum = $content["bank"]) || $sum = 0;
  $pdf->Cell(25, 10, decodeWin(utils()->getMoney($sum)), 0, 1, 'R');
  $pdf->SetFont('Arial', '', 10);

  $pdf->Cell(25, 8, 'Valeur', 0, 0, 'C');
  $pdf->Cell(30, 8, 'Nombre', 0, 0, 'L');
  $pdf->Cell(20, 8, 'Montant', 0, 1, 'C');
  $pdf->line($pdf->GetX(), $pdf->Gety() - 1, $pdf->GetX() + 75, $pdf->GetY() - 1);
  $pdf->SetFillColor(250);
  $pdf->SetDrawColor(200);
  foreach ($values as $idx => $value) {
    $pdf->Cell(20, 8, decodeWin(utils()->getMoney($value)), 0, 0, 'R');
    $pdf->Cell(5, 8, '');
    $pdf->Cell(18, 6, ($nbr = $content[$idx]) ? $nbr : '', 1, 0, 'R', true);
    $pdf->Cell(32, 8, $nbr ? decodeWin(utils()->getMoney($value * $nbr)) : '', 0, 1, 'R');
    $sum += $value * $nbr;
    $pdf->line($pdf->GetX(), $pdf->Gety() - 1, $pdf->GetX() + 75, $pdf->Gety() - 1);
  }
  $pdf->SetFont('Arial', 'B', 10);
  $pdf->SetLeftMargin($pdf->getX() + 15);
  $pdf->Cell(35, 10, "Total caisse");
  $pdf->Cell(25, 10, decodeWin(utils()->getMoney($sum)), 0, 1, 'R');
  $pdf->Cell(35, 10, decodeWin("Feuille caisse"));
  $pdf->Cell(25, 10, decodeWin(utils()->getMoney($sum - $tup[2])), 0, 1, 'R');
  $tup[2] && $pdf->SetTextColor(200, 20, 20);
  $pdf->Cell(35, 10, decodeWin("Différence de caisse"));
  $pdf->Cell(25, 10, decodeWin(utils()->getMoney($tup[2])), 0, 1, 'R');

  $pdf->Output('I');
}

buildTableDefs();
$_REQUEST["content"] && printContent() || printCash();
