<?php

function updateTicketAttrRequest()
{
  //for all type CASH_TYPE_CAUTION, CASH_TYPE_RETURN,  CASH_TYPE_SALE, CASH_TYPE_TRANSFER
  switch (utils()->action) {
    case "insert":
    case "update":
      utils()->checkUserResponse();
      switch ($_REQUEST["val_type"] = $_REQUEST["type"]) {
        case CASH_TYPE_CAUTION:
          if ($_REQUEST["inOut"] == CASH_TYPE_RETURN) {
            $_REQUEST["val_unitary"] *= -1;
          }
          break;

        default:
          if (($tmp = &$_REQUEST["val_description"]) == IN_OUT_REFUSED || $tmp == IN_OUT_OTHER) {
            $tmp .= $_REQUEST["val_otherDesc"];
          }
          unset($_REQUEST["val_otherDesc"]);
          break;
      }
  }
}

trait DisplayLine
{

  static $transferCash = [
      1 => "Banque PA->Caisse PA",
      2 => "Caisse PA->Banque PA",
      3 => "Caisse buvette->Caisse PA",
      4 => "Caisse PA->Caisse buvette",
  ];
  protected $cautions;
  protected $sumOp = 0;
  static $cashType = [
      CASH_TYPE_SALE      => "Vente",
      CASH_TYPE_RETURN    => "Remb.",
      CASH_TYPE_CAUTION   => "Consigne",
      CASH_TYPE_TRANSFER  => "Transfert",
      CASH_TYPE_ADJUST    => "Contenu",
      CASH_TYPE_ROUNDED   => "Arrondis",
      CASH_TYPE_INIT      => "Clôture",
      CASH_TYPE_AUTO_INIT => "Auto init",
      CASH_TYPE_TICKET    => "Opération"
  ];
  static $inOutReason = [
      IN_OUT_MISSING   => "Manquant",
      IN_OUT_DAMAGING  => "Abîmé",
      IN_OUT_TOO_SMALL => "Trop petit",
      IN_OUT_TOO_MUCH  => "En trop",
      IN_OUT_REFUSED   => "Refusé par client: ",
      IN_OUT_OTHER     => "Autre: "
  ];

  function updateValue($key, $row)
  {
    if ($key == "type") {
      return $this::$cashType[$row[$key]];
    }

    $mult = 1;
    switch ($row["type"]) {
      case CASH_TYPE_CAUTION :
        //Consigne
        switch ($key) {
          case "description":
            return $row["number"] . " X " . utils()->getMoney(abs($row["unitary"])) . ".";

          case "move":
            $this->sumOp += ($move = $row["number"] * $row["unitary"]);
            return $move;
        }
        break;

      case CASH_TYPE_RETURN:
        $mult = -1;
      case CASH_TYPE_SALE:
        //Remboursement ou vente
        switch ($key) {
          case "move":
            $this->sumOp += ($move = round($mult * ($row[$key] ?? $row["unitary"] * $row["number"]) * ($row["discount"] ? ($mult == 1 ? 100 - $row["discount"] : $row["discount"]) / 100 : 1), 2));
            return $move;

          case "description":
            $number = $row["number"];
            if ($number) {
              $unitary = $row["unitary"];
              $move = utils()->getMoney($unitary * $number);
              $unitary = utils()->getMoney($unitary);
            }
            else {
              $move = utils()->getMoney($row["move"]);
            }

            $ret = ($row["type"] == CASH_TYPE_RETURN && $row["command"] ? "N° BC " . $row["command"] . ". " : '') . $row["productNb"] . ' ' . $row["product"] . ($number > 1 ? ". ($number x $unitary). " : '. ');
            switch ($reason = $row[$key][0]) {
              case IN_OUT_REFUSED :
                $ret .= DisplayLine::$inOutReason[$reason];
              case IN_OUT_OTHER :
                $ret .= substr($row[$key], 1);
                break;

              default:
                if (is_numeric($reason)) {
                  $ret .= DisplayLine::$inOutReason[$reason];
                }
                else {
                  $ret .= $row[$key];
                }
                break;
            }
            if ($row["discount"]) {
              $ret .= " <br><b>Remise " . $row["discount"] . "% sur $move</b>";
            }
            return $ret;
        }
        break;

      case CASH_TYPE_TRANSFER:
        //Transfert
        switch ($key) {
          case "description":
            return DisplayLine::$transferCash[$row[$key]];

          case "move":
            return ($row["description"] % 2 == 0 ? -1 : 1) * $row[$key];
        }
        break;

      case CASH_TYPE_ADJUST:
        //Ajustement
        if ($key == "description") {
          $desc = json_decode($row["description"], true)["activityDate"];
          return $desc ? "Date activité: " . $desc : '';
        }
        break;

      case CASH_TYPE_ROUNDED:
        //Rounded
        switch ($key) {
          case "description":
            return '';

          case "move":
            $this->sumOp += $row[$key];
            break;
        }
        break;

      case CASH_TYPE_INIT:
        if (!utils()->isUserRole(ROLE_CASHIER) && $key == "description") {
          return "<a class=blue-text target=_blank href=cashPrint.php?current=" . $row["ri"] . "><i class='fas fa-print mr-2'></i>Imprimer feuille de caisse</a>";
        }
        break;

      case CASH_TYPE_TICKET:
        if ($key == "description") {
          $operator = dbUtil()->result(dbUtil()->selectRow("users",
                                                           "concat(" . (utils()->isSmartphoneEngine ? "substring(firstName, 1, 1), '. ', substring(name, 1, 1), '.')" : "firstName, ' ', name)"),
                                                           "ri=" . $row["user"], false), 0);
          $sumOp = $this->sumOp;
          $this->sumOp = 0;
          return ""
                  . "<div class='d-flex justify-content-between'>"
                  . "  <div><sub>$operator</sub></div>"
                  . "  <div class=font-weight-bold>Total client: <span class='ml-2 money-" . ($sumOp >= 0 ? "pos'>" : "neg'>") . utils()->getMoney($sumOp) . "</span></div>"
                  . "</div>";
        }
        break;
    }
    return $row[$key];
  }

}

class GetMove
{

  use DisplayLine;
}

function getCashAmout($last = null, $pr = null)
{

  $move = new GetMove;
  $last && $end = "ri<=$last and ";
  $res = dbUtil()->selectRow("cash", "ri", ($pr ? "pr=$pr and " : '') . "{$end}type = " . CASH_TYPE_INIT . " order by ri desc limit 1", !$pr);
  ($ri = dbUtil()->result($res, 0)) && $ri = "ri>=$ri";
  $ri = ($last ? "ri<=$last" : '1') . ($ri ? " and $ri" : '');
  $res = dbUtil()->selectRow("cash", "type, description, move, number, unitary, discount", ($pr ? "pr=$pr and " : '') . "$ri order by ri desc", !$pr);
  while ($tup = dbUtil()->fetch_assoc($res)) {
    $curSum += $move->updateValue("move", $tup);
  }
  return $curSum ? $curSum : 0;
}
