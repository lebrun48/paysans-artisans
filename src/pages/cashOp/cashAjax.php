<?php

function axCashSubmit()
{
  switch (utils()->action) {
    case "delete":
      updatePreviousContent(true);
      break;

    case "insert":
      $insertAction = true;
      $_REQUEST["val_time"] = utils()->now->format('H') * 10000 + utils()->now->format('i') * 100 + utils()->now->format('s');
    case "update":
      switch ($_REQUEST["val_type"] = $_REQUEST["type"]) {
        case CASH_TYPE_ADJUST:
          $desc = array();
          foreach ($_REQUEST as $k => $v) {
            if (substr($k, 0, 4) == "val_" && is_numeric($idx = substr($k, 4))) {
              if ($v) {
                $desc[$idx] = $v;
              }
              unset($_REQUEST[$k]);
            }
          }
          $desc["activityDate"] = $_REQUEST["activityDate"];
          $desc["bank"] = $_REQUEST["val_bank"];
          unset($_REQUEST["val_bank"]);
          $_REQUEST["val_description"] = json_encode($desc, JSON_NUMERIC_CHECK);
          break;

        case CASH_TYPE_INIT:
          $tup = dbUtil()->fetch_row(dbUtil()->selectRow("cash", "type, description, move", "1 order by ri desc limit 1"));
          if ($tup[0] != CASH_TYPE_ADJUST || !($date = json_decode($tup[1], true)["activityDate"])) {
            msgBox("Avant de clôturer la caisse, if faut faire le contenu de caisse <b>avec</b> une date d'activité.", "Erreur clôture", MODAL_SIZE_SMALL);
            exit();
          }
          $_REQUEST["val_move"] = getCashAmout();
          break;

        case CASH_TYPE_AUTO_INIT:
          if ($_REQUEST["val_updateType"]) {
            $_REQUEST["val_type"] = CASH_TYPE_INIT;
          }
          unset($_REQUEST["val_updateType"]);
          break;

        default:
          updateTicketAttrRequest();
          !$insertAction && updatePreviousContent();

          break;
      }
  }
  utils()->axRefreshElement("cash");
}

function updatePreviousContent($isDelete = false)
{
  $tup = dbUtil()->getCurrentEditedRow();

  class Diff
  {

    use DisplayLine;

    function getDiff($currentTup, $isDelete)
    {
      $current = $this->updateValue("move", $currentTup);
      foreach ($_REQUEST as $key => $val) {
        if (substr($key, 0, 4) == "val_") {
          $newTup[substr($key, 4)] = $val;
        }
      }
      !$isDelete && $new = $this->updateValue("move", $newTup);
      return $new - $current;
    }

  }

  $getDiff = new Diff;
  $diff = $getDiff->getDiff($tup, $isDelete);

  $diff && dbUtil()->updateRow("cash", "move=move-($diff)", "ri>" . $tup["ri"] . " and type=" . CASH_TYPE_ADJUST. " limit 1");
}

function axCashPrintLeaf()
{
  $tup = dbUtil()->fetch_row(dbUtil()->selectRow("cash", "type, description", "1 order by ri desc limit 1"));
  if ($tup[0] != CASH_TYPE_ADJUST || !$tup[1] || !json_decode($tup[1], true)["activityDate"]) {
    msgBox("Avant d'imprimer la feuille de caisse, il faut faire le contenu de caisse <b>avec</b> une date d'activité.", "Impression caisse", MODAL_SIZE_SMALL);
    exit();
  }
  utils()->axExecuteJS(["window.open", "cashPrint.php"]);
}
