<?php

function cashContentModalEdit($row = null)
{
  $curSum = getCashAmout($row ? $row["ri"] - 1 : null);
  if ($row) {
    $row["description"] = json_decode($row["description"], true);
  }
  if ($_REQUEST["action"] == "display") {
    $disabled = " disabled";
  }
  elseif (dbUtil()->updateRow("ticket", "user=" . utils()->userSession()["ri"], "pr=" . utils()->userSession()["pr"] . " and isTicket", false) &&
          dbUtil()->result(dbUtil()->selectRow("ticket", "count(*)", "1"), 0)) {
    include "ticketMain.php";
    msgbox("<p>Il y a encore des &laquo;Opérations client&raquo; non enregistrées. Certaines de ces opérations peuvent venir d'autres encodeurs du PR.</p>"
            . "<p>Toutes les opérations doivent être enregistrées ou supprimées avant de faire un décompte de caisse!</p>");
    unset($_REQUEST["action"]);
    unset(utils()->action);
    utils()->axRefreshElement("ticket");
    return;
  }

  $content = 0;
  msgBox(BuildForm::getForm([
              ED_FORM_ATTR    => "class='px-2 nofocus'",
              [ED_INSERT_HTML => "<input type=hidden name=type value=" . CASH_TYPE_ADJUST . ">Date activité"],
              "!activityDate" => [
                  ED_DIV_ATTR    => "class=mt-0",
                  ED_DISABLED    => !!$disabled,
                  ED_VALUE       => $row["description"]["activityDate"],
                  ED_TYPE        => ED_TYPE_DATETIME,
                  ED_PLACEHOLDER => "Choisir la date",
                  ED_DATETIME_PICKER => ["format" => "L"],
              ],
              [ED_INSERT_HTML => ""
                  . "<table class='table table-sm'>"
                  . "  <tr>"
                  . "  <tr>"
                  . "    <th colspan=2 style=text-align:center;font-weight:bold>EN BANQUE</th>"
                  . "    <th class=text-right><input$disabled class='inp-cash remove-stepper' type=number min=0 step=0.05 name=val_bank value='" . $row["description"]["bank"] . "' style=width:5em><b>€</b></th>"
                  . "  </tr>"
                  . "  <tr>"
                  . "    <th></th>"
                  . "    <th>Nombre</th>"
                  . "    <th>Montant</th>"
                  . "  </tr>"
                  . getTableContent($row, $disabled, $curSum, $content)
                  . "</table>"
                  . "<input type = hidden name = val_move value = " . ($content - $curSum) . ">"],
                  ], null, "contentForm") . ""
          . "<div class=card>"
          . "  <div class='card-body py-1 m-auto'>"
          . "    <table>"
          . "      <tr><th class='pr-2 font-weight-bold'>Caisse</th><td class=text-right  id=contentCash>" . utils()->getMoney($content) . "</td></tr>"
          . "      <tr><th class='pr-2 font-weight-bold'>Feuille caisse</th><td class=text-right id=contentCurrent data-content=$curSum>" . utils()->getMoney($curSum) . "</td></tr>"
          . "      <tr class='border-top border-light'><th class='pr-2 font-weight-bold'>Différence</th><td class=text-right  id=contentDiff><span class=money-" . (($diff = $content - $curSum) < 0 ? "neg" : "pos") . ">" . utils()->getMoney($diff) . "</span></td></tr>"
          . "    </table>"
          . "  </div>"
          . "</div>", "Contenu caisse", MODAL_SIZE_SMALL, [
      MSGBOX_BUTTON_CLOSE  => ["fermer", "class=px-3"],
      MSGBOX_BUTTON_ACTION => [!$disabled ? "Enregistrer" : null, "class=px-3"],
      MSGBOX_MODAL_ATTR    => [
          MODAL_FOOTER => ""
          . ($row ? getButton([
                      BUTTON_ICON_ONLY => "<i class='fas fa-print mr-1'></i>",
                      BUTTON_SIZE_SM   => true,
                      BUTTON_COLOR     => "btn-grey",
                      BUTTON_ATTR      => "id=printContent data-toggle=tooltip title='Impression du contenu' target=_blank href=cashPrint.php?content=" . $row["ri"]
                  ]) : '')
      ]
          ]
  );
}

function getTableContent($row, $disabled, $curSum, &$content)
{
  $values = array(/* 500, 200, 100, */3 => 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.05);
  ($content = $row["description"]["bank"]) || ($content = 0);
  foreach ($values as $k => $v) {
    $nb = $row["description"][$k];
    $ret .= ""
            . "<tr>"
            . " <th class = 'text-right py-0' data-val = $v>" . utils()->getMoney($v) . "</th>"
            . " <td class = py-0><input$disabled style=width:50px class='inp-cash remove-stepper' min=0 type=number step=1 name=val_$k value='$nb'></td>"
            . " <td class = 'text-right py-0'>" . ($nb ? utils()->getMoney($nb * $v) : '') . "</td>"
            . "</tr>";
    $content += $nb * $v;
  }
  return $ret;
}
