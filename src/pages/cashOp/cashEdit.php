<?php

include_once "common/src/BuildForm.php";

function axCashEdit()
{
  if (utils()->action == "delete") {
    msgbox([
        MSGBOX_SIZE          => MODAL_SIZE_SMALL,
        MSGBOX_TITLE         => "<h4 class=modal-title>Confirmation</h4>",
        MSGBOX_CONTENT       => "Confirmation de la suppression",
        MSGBOX_BUTTON_ACTION => "Supprimer",
        MSGBOX_BUTTON_CLOSE  => "Annuler",
    ]);
    return;
  }

  $row = dbUtil()->getCurrentEditedRow();
  $type = $row ? $row["type"] : $_REQUEST["type"];
  switch ($type) {
    case CASH_TYPE_TRANSFER:
      $title = "Mouvements spéciaux";
      $buttonLabel = "Enregistrer";
      break;

    case CASH_TYPE_AUTO_INIT:
      $title = "Auto-cloture caisse";
      $buttonLabel = "Enregistrer";
      break;

    case CASH_TYPE_SALE:
    case CASH_TYPE_RETURN:
    case CASH_TYPE_CAUTION:
      include "ticketEdit.php";
      $title = DisplayLine::$cashType[$type];
      $buttonLabel = "Enregister";
      $toCall = "buildTicketForm";
      $noSize = true;
      break;

    case CASH_TYPE_ADJUST:
      include "cashContentEdit.php";
      cashContentModalEdit($row);
      exit();

    default :
      msgBox("Ce type de ligne n'est pas encore éditable", $title, MODAL_SIZE_SMALL);
      return;
  }

  msgBox([
      MSGBOX_CONTENT       => $toCall ? buildTicketForm($row) : buildForm($row),
      MSGBOX_TITLE         => $title,
      MSGBOX_BUTTON_ACTION => $buttonLabel,
      MSGBOX_BUTTON_CLOSE  => "Annuler",
      MSGBOX_SIZE          => $noSize ? null : MODAL_SIZE_SMALL
  ]);
  exit();
}

function buildForm($row = null)
{
  $type = $row ? $row["type"] : $_REQUEST["type"];
  switch ($type) {
    case CASH_TYPE_TRANSFER :
      $col = array(
          "!type"       => array(
              ED_TYPE  => ED_TYPE_HIDDEN,
              ED_VALUE => CASH_TYPE_TRANSFER
          ),
          "description" => array(
              ED_FIELD_WIDTH => 12,
              ED_PLACEHOLDER => "Choisir le type d'opération",
              ED_LABEL       => "Opération",
              ED_TYPE        => ED_TYPE_SELECT,
              ED_OPTIONS     => array(
                  array(
                      ED_VALUE   => 1,
                      ED_CONTENT => DisplayLine::$transferCash[1]
                  ),
                  array(
                      ED_VALUE   => 2,
                      ED_CONTENT => DisplayLine::$transferCash[2]
                  ),
                  array(
                      ED_VALUE   => 3,
                      ED_CONTENT => DisplayLine::$transferCash[3]
                  ),
                  array(
                      ED_VALUE   => 4,
                      ED_CONTENT => DisplayLine::$transferCash[4]
                  )
              ),
              ED_VALIDATE    => array(
                  ED_VALIDATE_REQUIRED => true,
              )
          ),
          "move"        => array(
              ED_TYPE       => ED_TYPE_FLOAT,
              ED_LABEL      => "Montant",
              ED_NO_STEPPER  => true,
              ED_TEXT_AFTER => '<i class="fas fa-euro-sign"></i>',
              ED_VALIDATE   => array(
                  ED_VALIDATE_REQUIRED  => true,
                  ED_VALIDATE_INVALIDE  => "Ben quoi...",
                  ED_VALIDATE_MIN_VALUE => 0.1,
                  ED_VALIDATE_MAX_VALUE => 10000,
              )
          )
      );
      break;

    case CASH_TYPE_INIT:
      $col = array(
          "!type" => array(
              ED_TYPE  => ED_TYPE_HIDDEN,
              ED_VALUE => CASH_TYPE_INIT
          ),
          "move"  => array(
              ED_TYPE       => ED_TYPE_FLOAT,
              ED_LABEL      => "Montant actuel",
              ED_TEXT_AFTER => '<i class="fas fa-euro-sign"></i>',
              ED_VALIDATE   => array(
                  ED_VALIDATE_REQUIRED  => true,
                  ED_VALIDATE_INVALIDE  => "Montant compris entre 0 et 10000",
                  ED_VALIDATE_MIN_VALUE => 0,
                  ED_VALIDATE_MAX_VALUE => 10000,
              )
          )
      );
      break;

    case CASH_TYPE_AUTO_INIT:
      $col = array(
          "!type"      => array(
              ED_TYPE  => ED_TYPE_HIDDEN,
              ED_VALUE => CASH_TYPE_AUTO_INIT
          ),
          "move"       => array(
              ED_TYPE       => ED_TYPE_FLOAT,
              ED_LABEL      => "Montant actuel",
              ED_TEXT_AFTER => '<i class="fas fa-euro-sign"></i>',
              ED_VALIDATE   => array(
                  ED_VALIDATE_REQUIRED  => true,
                  ED_VALIDATE_INVALIDE  => "Montant compris entre 0 et 10000",
                  ED_VALIDATE_MIN_VALUE => 0,
                  ED_VALIDATE_MAX_VALUE => 10000,
              )),
          "updateType" => array(
              ED_TYPE    => ED_TYPE_CHECK,
              ED_LABEL   => "Modifier en 'Init'",
              ED_CHECKED => true,
              ED_VALUE   => 1
          )
      );
      break;
  }
  return BuildForm::getForm($col, $row);
}
