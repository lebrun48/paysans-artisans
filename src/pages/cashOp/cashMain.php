<?php

include_once "DisplayLine.php";

function buildCashBox()
{
  echo ""
  . "<div class=col-lg-" . (utils()->roles[ROLE_CASHIER] ? '6' : '12') . " id=cashBox>"
  . "  <div class = 'card mt-3'>"
  . "    <div class = 'card-body'>"
  . "      <h5 class='text-center card-title'>Mouvements de Caisse</h5>";
  if (!utils()->isUserRole(ROLE_CASHIER)) {
    $ddMenu = [
        ED_NAME    => "opCash",
        ED_ATTR    => "style=color:black;background:#ffc107 class='btn-sm'",
        ED_CONTENT => "Opérations",
        ED_OPTIONS => [
            ED_ATTR => "class=dropdown-warning",
            [
                ED_CONTENT => "Mouvements spéciaux",
                ED_TYPE    => ED_TYPE_BUTTON,
                ED_ATTR    => "id=cashMove"
            ],
            [
                ED_CONTENT => "Contenu caisse",
                ED_TYPE    => ED_TYPE_BUTTON,
                ED_ATTR    => "id=cashContent"
            ],
            [
                ED_CONTENT => "Cloture caisse",
                ED_TYPE    => ED_TYPE_BUTTON,
                ED_ATTR    => "id=cashInit"
            ],
        ]
    ];
    buildDropDown($ddMenu);
  }



  $table = new Cash("cash");
  $table->enableSearch = false;
  $table->theadAttr = "class = 'grey lighten-4'";
  $table->isStriped = false;
  $table->setDefaultSort("time desc, user, ri asc");
  $table->otherDbCol = "discount, number, unitary, command, product, user";
  $_REQUEST["scrollTop"] && $table->defaultWhere = "ri<=" . $_REQUEST["scrollTop"];
  $table->buildFullTable();

  echo ""
  . "    </div>"
  . "    <div class='d-flex flex-row-reverse pr-2'>" . getPagination($_REQUEST["scrollTop"], $table->closureRi) . "</div>"
  . "  </div>"
  . "</div>";
  utils()->insertReadyFunction("cashListener");
}

function getPagination($current, $last)
{
  $ret = ""
          . "<nav>"
          . "  <ul class='pagination'>"
          . "    <li class='page-item'><a onclick=axExecute('prevPage',{scrollTop:$current}) class='page-link page-prev border"
          . (!$current ? " disabled" : '') . "'><i class='fas fa-chevron-left fa-lg" . (!$current ? " text-black-50" : '') . " '></i></a></li>"
          . "    <li class='page-item'><div class='page-link border grey lighten-4'>Clôtures</div></li>"
          . "    <li class='page-item'><a onclick=axExecute('nextPage',{scrollTop:$last}) class='page-link page-next border"
          . (!$last ? " disabled" : '') . "'><i class='fas fa-chevron-right fa-lg" . (!$last ? " text-black-50" : '') . "'></i></a></li>"
          . "  </ul>"
          . "</nav>  ";
  return $ret;
}

class Cash extends BuildTable
{

  use DisplayLine;

  public $first = true;
  public $stopNext;
  public $closureRi;

  function buildBeforeTable()
  {
    echo "<div class = 'text-right'>Total: <b class = money-pos>" . utils()->getMoney(getCashAmout($_REQUEST["scrollTop"])) . "</b></div>";
  }
  
  function buildInsertAction()
  {
  }

  function buildLine($row)
  {
    if ($this->stopNext || $row["type"] == CASH_TYPE_INIT && !$this->first && $this->closureRi = $row["ri"]) {
      return true;
    }
    $this->stopNext = $this->first && !$_REQUEST["scrollTop"] && $row["type"] == CASH_TYPE_INIT && $this->closureRi = $row["ri"];
    $this->first = false;
    parent::buildLine($row);
  }

  function getDisplayValue($key, $row)
  {
    $ret = $this->updateValue($key, $row);
    if ($key == "move") {
      $this->sumCash += $ret;
      if ($row["type"] == CASH_TYPE_TICKET) {
        return '';
      }
    }
    return $ret;
  }
  
  function getMoney($value, $row, $key)
  {
    if ($key == "move" && $row["type"] == CASH_TYPE_TICKET) {
      return '';
    }
    return parent::getMoney($value, $row, $key);
  }

  public function getRightsOnLine($row, $currentRights)
  {
    if ($row["type"] == CASH_TYPE_TICKET) {
      return $this->sumOp ? 0 : TABLE_RIGHT_DELETE;
    }
    if ($this->defaultWhere) {
      return $row["type"] == CASH_TYPE_ADJUST ? TABLE_RIGHT_EDIT + TABLE_RIGHT_ONLY_DISPLAY : 0;
    }
    if (utils()->isUserRole(ROLE_CASHIER) && $row["user"] != utils()->userSession()["ri"]) {
      return 0;
    }
    if ($currentRights != TABLE_RIGHT_ONLY_DISPLAY && ($row["type"] == CASH_TYPE_ROUNDED || $row["type"] == CASH_TYPE_INIT)) {
      return TABLE_RIGHT_DELETE;
    }
    return parent::getRightsOnLine($row, $currentRights);
  }

}
