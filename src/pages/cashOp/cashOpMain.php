<?php

include "cashMain.php";
include "ticketMain.php";

function buildMain()
{
//  dbUtil()->seeRequest=1;
//  $res = dbUtil()->query("select ri, description from cash where type=" . CASH_TYPE_ADJUST);
//  while ($tup = dbUtil()->fetch_row($res)) {
//    if ($tup[1][0] != '{') {
//      $description = json_encode(unserialize($tup[1]), JSON_NUMERIC_CHECK);
//      dbUtil()->query("update cash set description='$description' where ri=$tup[0]");
//    }
//  }
//  exit();
  echo ""
  . "<main id='main'>"
  . "  <div class=row>";
  utils()->roles[ROLE_CASHIER] && buildTicketBox();
  buildCashBox();
  echo ""
  . "  </div>"
  . "</main>";
}

function axCashOpValidate()
{
  if (strlen($_REQUEST["sum"])) {
    $time = utils()->now->format('H') * 10000 + utils()->now->format('i') * 100 + utils()->now->format('s');
    dbUtil()->updateRow("ticket", ["isTicket" => 0, "time" => $time]);
    if ($round = utils()->roundMoney($_REQUEST["sum"]) - round($_REQUEST["sum"], 2)) {
      dbUtil()->insertRow("cash", ["type" => CASH_TYPE_ROUNDED, "time" => $time, "move" => $round]);
    }
    dbUtil()->insertRow("cash", ["type" => CASH_TYPE_TICKET, "time" => $time, "description" => round($_REQUEST["sum"], 2) + $round]);
    utils()->axRefreshMain();
  }
}

function axGetProduct()
{
  $dbIdx = DBUtil::connectDB("dbproducts");
  dbUtil($dbIdx)->tables["products"] = [DB_TABLENAME => "_raliments"];
  $productNb = preg_replace('/([,:;\s-])/', '.', $_REQUEST["nb"]);
  $tup = dbUtil($dbIdx)->fetch_assoc(dbUtil($dbIdx)->selectRow("products", "label, if(cb2c=0, 1, cb2c) as cb2c, date_price, price_ttc, tva_tx, deposit_return", "ref='$productNb'"));
  if (!$tup && strpos($productNb, '.') === false) {
    $sep = [6 => [[2, 2]], 7 => [[2, 2], [2, 3], [3, 2]], 8 => [[2, 3], [3, 2], [3, 3]], 9 => [[3, 3]]];
    if (($sz = strlen($productNb)) >= 6 && $sz <= 9) {
      foreach ($sep[$sz] as $ar) {
        $offset = 0;
        $newPn = '';
        foreach ($ar as $length) {
          $newPn .= substr($productNb, $offset, $length) . '.';
          $offset += $length;
        }
        $newPn .= substr($productNb, $offset);
        if ($tup = dbUtil($dbIdx)->fetch_assoc(dbUtil($dbIdx)->selectRow("products", "label, if(cb2c=0, 1, cb2c) as cb2c, date_price, price_ttc, tva_tx, deposit_return", "ref='$newPn'"))) {
          $productNb = $newPn;
          break;
        }
      }
    }
  }
  if (!$_REQUEST["lastPrice"]) {
    $nbDays = utils()->now()->format('w');
    $minDate = utils()->now()->sub(new DateInterval("P{$nbDays}D"));
    $minDate->setTime(00, 00);
  };

  $product = [
      "ed_productNb" => $productNb,
      "ed_tva"       => $tup["tva_tx"] == 21 ? 2 : 1,
      "ed_product"   => $tup["label"],
      "ed_unitary"   => $_REQUEST["lastPrice"] || ($dt = new DateTime($tup["date_price"])) < $minDate ? round($tup["price_ttc"] * $tup["cb2c"], 2) : '',
      "ed_caution"   => substr($tup["deposit_return"], 3) / 100
  ];
  !$product["ed_unitary"] && $product["ed_unitary"] = '';

  utils()->axExecuteJS("updateProduct", $product);
}

function axNextPage()
{
  utils()->axRefreshElement("cash");
}

function axPrevPage()
{
  $_REQUEST["scrollTop"] = dbUtil()->result(dbUtil()->selectRow("cash", "ri", "ri>" . $_REQUEST["scrollTop"] . " and type=" . CASH_TYPE_INIT . " limit 1"), 0);
  utils()->axRefreshElement("cash");
}
