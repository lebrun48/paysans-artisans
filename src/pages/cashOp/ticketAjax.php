<?php

function axTicketSubmit()
{
  updateTicketAttrRequest();
  utils()->axRefreshElement("ticket");
}

function axTicketCancel()
{
  msgBox([
      MSGBOX_BUTTON_ACTION => ["Supprimer", "onclick=axExecute({xAction:'erase',page:'ticket'})"],
      MSGBOX_BUTTON_CLOSE  => "Annuler",
      MSGBOX_TITLE         => "Confirmation",
      MSGBOX_CONTENT       => "<p>Supprimer le ticket?</p>",
      MSGBOX_SIZE          => MODAL_SIZE_SMALL
  ]);
}

function axTicketErase()
{
  dbUtil()->deleteRow("ticket");
  utils()->axRefreshElement("ticket");
}
