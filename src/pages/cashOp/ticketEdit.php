<?php

include_once "common/src/BuildForm.php";

function axTicketEdit()
{
  if (utils()->action == "delete") {
    utils()->axRefreshElement("ticket");
    return;
  }

  $row = dbUtil()->getCurrentEditedRow();
  $type = $row ? $row["type"] : $_REQUEST["type"];
  switch ($type) {
    case CASH_TYPE_SALE:
      $title = "Vente";
      break;

    case CASH_TYPE_RETURN:
      $title = "Remboursement";
      break;

    case CASH_TYPE_CAUTION:
      $title = "Consigne";
      break;
  }

  msgBox([
      MSGBOX_CONTENT       => buildTicketForm($row),
      MSGBOX_TITLE         => $title,
      MSGBOX_BUTTON_ACTION => "Enregister",
      MSGBOX_BUTTON_CLOSE  => "Annuler",
  ]);
  exit();
}

function buildTicketForm($row = null)
{
  $type = $row ? $row["type"] : $_REQUEST["type"];
  if ($type == CASH_TYPE_CAUTION) {
    if ($row) {
      $row["inOut"] = $row["unitary"] < 0 ? CASH_TYPE_RETURN : CASH_TYPE_SALE;
      $row["unitary"] = abs($row["unitary"]);
    }

    $col = [
        "!type"   => [
            ED_TYPE  => ED_TYPE_HIDDEN,
            ED_VALUE => $type
        ],
        "!inOut"  => [
            ED_FIELD_WIDTH => 6,
            ED_NEXT_LINE   => true,
            ED_TYPE        => ED_TYPE_RADIO,
            ED_OPTIONS     => [
                [
                    ED_VALUE   => CASH_TYPE_RETURN,
                    ED_LABEL   => "Retour",
                    ED_CHECKED => !$row || $row["inOut"] == CASH_TYPE_RETURN,
                ],
                [
                    ED_VALUE   => CASH_TYPE_SALE,
                    ED_LABEL   => "Vente",
                    ED_CHECKED => $row["inOut"] == CASH_TYPE_SALE,
                ]
            ]
        ],
        "unitary" => [
            ED_FIELD_WIDTH => 6,
            ED_TYPE        => ED_TYPE_SELECT,
            ED_ATTR        => "data-visible-options=6",
            ED_VALIDATE    => [
                ED_VALIDATE_REQUIRED => true,
                ED_VALIDATE_INVALIDE => "Sélectionnez le montant de la consigne",
            ],
            ED_PLACEHOLDER => "Montant",
            ED_LABEL       => "Consigne",
            ED_OPTIONS     => [
                [
                    ED_VALUE   => 0.1,
                    ED_CONTENT => "0,10€"],
                [
                    ED_VALUE   => 0.3,
                    ED_CONTENT => "0,30€"],
                [
                    ED_VALUE   => 0.5,
                    ED_CONTENT => "0,50€"],
                [
                    ED_VALUE   => 1,
                    ED_CONTENT => "1,00€"],
                [
                    ED_VALUE   => 2,
                    ED_CONTENT => "2,00€"],
                [
                    ED_VALUE   => 3,
                    ED_CONTENT => "3,00€"],
            ],
        ],
        "number"  => [
            ED_FIELD_WIDTH => 6,
            ED_LABEL       => "Nombre",
            ED_TYPE        => ED_TYPE_INTEGER,
            ED_ATTR        => "onkeyup=computeCaution(]",
            ED_VALIDATE    => [
                ED_VALIDATE_MAX_LENGTH => 3,
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_MIN_VALUE  => 1,
                ED_VALIDATE_MAX_VALUE  => 100,
                ED_VALIDATE_INVALIDE   => "Entrez le nombre de consignes (max 100)"
            ]
        ],
    ];
  }
  else {
    $col1 = [
        "!type"       => [
            ED_TYPE  => ED_TYPE_HIDDEN,
            ED_VALUE => $type
        ],
//        "command"     => [
//            ED_FIELD_WIDTH => 3,
//            ED_LABEL       => "N° Commande",
//            ED_TYPE        => ED_TYPE_INTEGER,
//            ED_STEP_NUMBER => 0,
//            ED_VALIDATE    => [
//                ED_VALIDATE_MAX_LENGTH => 5,
//                ED_VALIDATE_MAX_VALUE  => 99999,
//                ED_VALIDATE_INVALIDE   => "Bon de commande invalide ou manquant",
//            ]
//        ],
        "productNb"   => [
            ED_FIELD_WIDTH => 3,
            ED_LABEL       => "N° produit",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_ATTR        => "inputmode=numeric",
            ED_VALIDATE    => [
                ED_VALIDATE_MAX_LENGTH => 15,
            ]],
        "product"     => [
            ED_FIELD_WIDTH => 9,
            ED_LABEL       => "Produit",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_VALIDATE    => [
                ED_VALIDATE_MAX_LENGTH => 50,
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_INVALIDE   => "Entrez le nom du produit",
            ]],
        "description" => [
            ED_FIELD_WIDTH => 6,
            ED_LABEL       => "Raison " . ($type == CASH_TYPE_SALE ? "vente" : "remboursement"),
            ED_PLACEHOLDER => "Choisir la raison",
            ED_TYPE        => ED_TYPE_SELECT,
            ED_ATTR        => "onchange=\"toggleSlide('otherDesc',this.options[selectedIndex].value>=" . (IN_OUT_REFUSED - ($type == CASH_TYPE_SALE ? 0 : isset($row))) . ",{required:true})\"",
            ED_VALUE       => IN_OUT_MISSING,
            ED_OPTIONS     => [
                [ED_CONTENT => DisplayLine::$inOutReason[IN_OUT_MISSING], ED_VALUE => IN_OUT_MISSING],
                [ED_CONTENT => DisplayLine::$inOutReason[IN_OUT_DAMAGING], ED_VALUE => IN_OUT_DAMAGING],
                [ED_CONTENT => DisplayLine::$inOutReason[IN_OUT_TOO_SMALL], ED_VALUE => IN_OUT_TOO_SMALL],
                [ED_CONTENT => DisplayLine::$inOutReason[IN_OUT_REFUSED], ED_VALUE => IN_OUT_REFUSED],
                [ED_CONTENT => DisplayLine::$inOutReason[IN_OUT_OTHER], ED_VALUE => IN_OUT_OTHER],
            ],
        ],
        "otherDesc"   => [
            ED_FIELD_WIDTH => 6,
            ED_LABEL       => "Autre raison",
            ED_TYPE        => ED_TYPE_ALPHA,
            ED_SLIDE_UP    => true,
            ED_VALIDATE    => [
                ED_VALIDATE_INVALIDE => "Entrez la raison " . ($type == CASH_TYPE_SALE ? "de la vente" : "du remboursement")
            ]
        ],
        "unitary"     => [
            ED_FIELD_WIDTH     => 4,
            ED_LABEL           => "Prix unitaire",
            ED_TYPE            => ED_TYPE_FLOAT,
            ED_NO_STEPPER      => true,
            ED_SELECT_ON_FOCUS => true,
            ED_TEXT_AFTER      => '<i class="fas fa-euro-sign"></i>',
            ED_VALIDATE        => [
                ED_VALIDATE_MAX_LENGTH => 6,
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_INVALIDE   => "Entrez le prix unitaire.",
                ED_VALIDATE_MIN_VALUE  => 0,
                ED_VALIDATE_MAX_VALUE  => 1000,
            ]],
        "number"      => [
            ED_FIELD_WIDTH     => 4,
            ED_LABEL           => "Nombre",
            ED_TYPE            => ED_TYPE_INTEGER,
            ED_VALUE           => 1,
            ED_SELECT_ON_FOCUS => true,
            ED_VALIDATE        => [
                ED_VALIDATE_REQUIRED   => true,
                ED_VALIDATE_MAX_LENGTH => 2,
                ED_VALIDATE_INVALIDE   => "Valeur entre 1 et 99",
                ED_VALIDATE_MIN_VALUE  => 1,
                ED_VALIDATE_MAX_VALUE  => 99,
            ]],
        "discount"    => [
            ED_FIELD_WIDTH => 4,
            ED_LABEL       => "Remise",
            ED_TYPE        => ED_TYPE_FLOAT,
            ED_NO_STEPPER  => true,
            ED_TEXT_AFTER  => '<i class="fas fa-percent"></i>',
            ED_VALIDATE    => [
                ED_VALIDATE_MAX_LENGTH => 6,
                ED_VALIDATE_MIN_VALUE  => 1,
                ED_VALIDATE_MAX_VALUE  => 99,
            ]],
        "!caution"    => [
            ED_FIELD_WIDTH => 6,
            ED_TYPE        => ED_TYPE_SELECT,
            ED_ATTR        => "data-visible-options=-1",
            ED_LABEL       => "Consigne",
            ED_NEXT_LINE   => true,
            ED_ALIGN_ELEMENT_AFTER  => "<div class='text-nowrap ml-2'>par unité</div>",
            ED_OPTIONS     => [
                [
                    ED_VALUE   => 0,
                    ED_CONTENT => " - - - "],
                [
                    ED_VALUE   => 0.1,
                    ED_CONTENT => "0,10€"],
                [
                    ED_VALUE   => 0.3,
                    ED_CONTENT => "0,30€"],
                [
                    ED_VALUE   => 0.5,
                    ED_CONTENT => "0,50€"],
                [
                    ED_VALUE   => 1,
                    ED_CONTENT => "1,00€"],
                [
                    ED_VALUE   => 2,
                    ED_CONTENT => "2,00€"],
                [
                    ED_VALUE   => 3,
                    ED_CONTENT => "3,00€"],
            ],
        ],
        "tva"         => [
            ED_FIELD_WIDTH => 6,
            ED_TYPE        => ED_TYPE_RADIO,
            ED_OPTIONS     => [
                [ED_VALUE => 1, ED_LABEL => 'TVA 6%', ED_CHECKED => true],
                [ED_VALUE => 2, ED_LABEL => 'TVA 21%'],
            ]
        ]
    ];

    if ($type == CASH_TYPE_SALE) {
      //vente
      unset($col1["command"]);
      unset($col1["description"][ED_OPTIONS]);
      $col1["description"][ED_OPTIONS] = [
          [ED_CONTENT => DisplayLine::$inOutReason[IN_OUT_TOO_MUCH], ED_VALUE => IN_OUT_TOO_MUCH],
          [ED_CONTENT => DisplayLine::$inOutReason[IN_OUT_OTHER], ED_VALUE => IN_OUT_OTHER],
      ];
      unset($col1["description"][ED_PLACEHOLDER]);
    }

    if ($row) {
      unset($col1["!caution"]);
      if ($row["description"][0] >= IN_OUT_REFUSED) {
        $row["otherDesc"] = substr($row["description"], 1);
        $row["description"] = $row["description"][0];
        $col1["otherDesc"][ED_VALIDATE][ED_VALIDATE_REQUIRED] = true;
        unset($col1["otherDesc"][ED_SLIDE_UP]);
        $col1["otherDesc"][ED_SLIDE_DOWN] = true;
      }
    }
  }
  return BuildForm::getForm($type == CASH_TYPE_CAUTION ? $col : $col1, $row, 'ticketForm');
}
