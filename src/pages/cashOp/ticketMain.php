<?php

include_once "DisplayLine.php";

function buildTicketBox()
{
//ticket
  echo ""
  . "<div class=col-lg-6 id=ticketBox>"
  . "  <div class='card mb-5 mt-3' style=min-height:60vh>"
  . "    <div class='card-body'>"
  . "      <h5 class='text-center card-title'>Opérations Client</h5>"
  . "      <div class='mb-1 text-right'>"
  . "        <button type=button style=width:" . (utils()->isSmartphoneEngine ? "80px" : "110px") . " id='ticketSale' class='waves-effect btn btn-sm btn-outline-green btn-rounded px-2 px-lg-4'>Vente</button>"
  . "        <button type=button style=width:" . (utils()->isSmartphoneEngine ? "80px" : "110px") . " id='ticketReturn' class='waves-effect btn btn-sm btn-outline-red btn-rounded px-2 px-lg-4'>Remb.</button>"
  . "        <button type=button style=width:" . (utils()->isSmartphoneEngine ? "80px" : "110px") . " id='ticketCaution' class='waves-effect btn btn-sm btn-outline-orange btn-rounded px-2 px-lg-4'>Consigne</button>"
  . "      </div>";
  $table = new TicketTable("ticket");
  $table->theadAttr = "class='grey lighten-4'";
  $table->enableSearch = false;
  $table->isStriped = false;
  $table->otherDbCol = "unitary, number, command, product, productNb, discount";
  $table->setDefaultSort("ri desc");
  $table->buildFullTable();
  $realValue = $table->sum;
  $table->sum = utils()->roundMoney($table->sum);
  echo ""
  . "      <div class='d-flex justify-content-center'>"
  . "        <div class=return>" . ($table->sum < 0 ? "A rendre " : "A recevoir ") . "</div>"
  . "        <div class=return>" . utils()->getMoney(abs($table->sum)) . "</div>"
  . "      </div>"
  . "      <div class='d-flex justify-content-center'>"
  . "        <div class=return style=padding-top:3px>Reçu client</div>"
  . "        <div class=return><input class=return id=clientReceived type=number data-sum=" . $table->sum . " step=0.05></div>"
  . "      </div>"
  . "      <div class='d-flex justify-content-center'>"
  . "        <div class=return id=toReturnOrReceive>" . ($table->sum < 0 ? "A rendre " : "A recevoir ") . "</div>"
  . "        <div class=return id=clientReturn>" . utils()->getMoney(abs($table->sum)) . "</div>"
  . "      </div>"
  . "    </div>"
  . "    <div class=card-footer>"
  . "      <div id=cautionButtons class=text-right>"
  . "      <button type=button id=ticketCancel class='btn btn-sm btn-outline-danger btn-rounded waves-effect'><i class='far fa-trash-alt'></i></button>"
  . "      <button type=button class='btn btn-sm btn-" . SITE_DEFAULT_COLOR . " btn-rounded waves-effect' "
  . "    onclick=axExecute({xAction:'validate',sum:'$realValue',page:'cashOp'})><i class='fas fa-check pr-2'></i>Enregistrer</button>"
  . "    </div>"
  . "  </div>"
  . "</div></div>";
  utils()->insertReadyFunction("ticketListener");
}

class TicketTable extends BuildTable
{

  use DisplayLine;

  function DBUpdateTerminated($action)
  {
    if ($action == "insert" && ($caution = $_REQUEST["caution"])) {
      dbUtil()->insertRow("ticket", ["type" => 3, "unitary" => $caution * ($_REQUEST["type"] == 2 ? -1 : 1), "number" => $_REQUEST["val_number"]]);
    }
  }

  function buildInsertAction()
  {
    
  }

  function getDisplayValue($key, $row)
  {
    $ret = $this->updateValue($key, $row);
    if ($key == "move") {
      $this->sum += $ret;
    }
    return $ret;
  }

  function buildLastLine()
  {
    echo "<tr><td colspan=2 class='text-right font-weight-bold'>Total (arrondi)</td><td class='text-right font-weight-bold "
    . ($this->sum >= 0 ? "money-pos" : "money-neg") . "'>" . utils()->getMoney(utils()->roundMoney($this->sum)) . "</td>";
    parent::buildLastLine();
  }

}
