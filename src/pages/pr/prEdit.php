<?php

include "common/src/BuildForm.php";

function axPrEdit()
{
  if (utils()->action == 'delete') {
    if (dbUtil()->hasExternalRef("pr", "'" . $_REQUEST["primary"][0] . "'")) {
      msgBox("<p>Il existe encore des utilisateurs sur ce point de R'Aliment.</p> Il ne peut être supprimé! ", "Erreur suppression", MODAL_SIZE_SMALL);
      return;
    }
    msgBox([
        MSGBOX_TITLE         => "Confirmation",
        MSGBOX_CONTENT       => "<p>Voulez-vous supprimer ce point de R'Aliment?</p>",
        MSGBOX_SIZE          => MODAL_SIZE_SMALL,
        MSGBOX_BUTTON_ACTION => "Oui",
        MSGBOX_BUTTON_CLOSE  => "Non"
            ]
    );
    return;
  }

  //insert or update/display
  $row = dbUtil()->getCurrentEditedRow();
  $arForm = [
      ED_FORM_ATTR  => "class='px-3 my-0'",
      "name"        => [
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_LABEL    => "Nom du PR",
          ED_VALIDATE => [
              ED_VALIDATE_INVALIDE   => "Doit contenir entre 1 et 20 caractères",
              ED_VALIDATE_REQUIRED   => true,
              ED_VALIDATE_MAX_LENGTH => 20,
          ]
      ],
      "cashLimit"   =>
      [
          ED_TYPE     => ED_TYPE_INTEGER,
          ED_LABEL    => "Limite de caisse",
          ED_NO_STEPPER  => true,
          ED_VALIDATE => [
              ED_VALIDATE_INVALIDE  => "Maximum 1000 (pas de décimal!)",
              ED_VALIDATE_MAX_VALUE => "1000",
          ]
      ],
      "responsible" =>
      [
          ED_TYPE        => ED_TYPE_SELECT,
          ED_LABEL       => "Responsable du PR",
          ED_OPTIONS     => getResponsibleList($row["ri"])
      ]
  ];

  if (!$row) {
    unset($arForm["responsible"]);
  }

  msgBox([
      MSGBOX_TITLE         => ($row ? "Modification" : "Création") . " PR",
      MSGBOX_SIZE          => MODAL_SIZE_SMALL,
      MSGBOX_CONTENT       => BuildForm::getForm($arForm, $row),
      MSGBOX_BUTTON_ACTION => "Enregistrer",
      MSGBOX_BUTTON_CLOSE  => "Annuler",
  ]);
}

function getResponsibleList($pr)
{
  if (!$pr) {
    return;
  }

  $options[] = [ED_VALUE => "=null", ED_CONTENT => '(Personne)'];
  $res = dbUtil()->selectRow("users", "ri, concat(firstName, ' ', upper(name))", "pr=$pr");
  while ($tup = dbUtil()->fetch_row($res)) {
    $options[] = [ED_VALUE => $tup[0], ED_CONTENT => $tup[1]];
  }
  return $options;
}
