<?php

include "../cashOp/DisplayLine.php";

function buildMain()
{
  echo ""
  . "<main id='main'>";
  buildPrBox();
  echo ""
  . "</main>";
}

function buildPrBox()
{
  echo ""
  . "<div id=prBox class='card mt-3'>"
  . "  <div class=card-body>"
  . "    <h5 class='text-center card-title m-0'>Mise à jour Points de R'Aliment</h5>";

  $table = new PR("pr");
  utils()->isSmartphoneEngine && ($table->tableAttr = "class=table-responsive");
  $table->theadAttr = "class='grey lighten-4'";
  $table->setDefaultSort("name asc");
  $table->buildFullTable();

  echo ""
  . "  </div>"
  . "</div>";
  utils()->insertReadyFunction("prListener");
}

class PR extends BuildTable
{

  public $closures;

  function getTrAttributes($row = null)
  {
    return "class=text-nowrap";
  }

  function getDisplayValue($key, $row)
  {
    switch ($key) {
      case "cashContent" :
        $amount = getCashAmout(null, $row["ri"]);
        $color = $amount > $row["cashLimit"] * 1.10 ? "green" : ($amount <= $row["cashLimit"] ? "red" : "orange");
        return "<span style=color:$color;font-weight:bold>" . utils()->getMoney($amount, true, true) . "</span>";

      case "lastClosure" :
        if (!($this->closures = dbUtil()->fetch_all(dbUtil()->selectRow("cash", "ri", "pr=" . $row["ri"] . " and type=" . CASH_TYPE_INIT . " order by ri desc limit 50", false)))) {
          return '';
        }
        $date = json_decode(dbUtil()->result(dbUtil()->selectRow("cash", "description", "pr=" . $row["ri"] . " and ri<" . $this->closures[0][0] . " order by ri desc limit 1", false), 0), true)["activityDate"];
        $data = $this->closures[0][0];
        unset($this->closures[0]);
        return "<a class=blue-text id=lastClosure data-value=$data><i class='fas fa-door-closed mr-1'></i>$date</a>";

      case "cashSheet":
        $line = "<select name=cash-sheet><option style=color:lightgray>Date activité</option>";
        foreach ($this->closures as $tup) {
          $date = json_decode(dbUtil()->result(dbUtil()->selectRow("cash", "description", "pr=" . $row["ri"] . " and ri<" . $tup[0] . " order by ri desc limit 1", false), 0), true)["activityDate"];
          !$this->lastClosure && $this->lastClosure = [$tup[0], $date];
          $line .= "<option value=" . $tup[0] . ">$date</option>";
        }
        return "$line</select>";
    }
    return parent::getDisplayValue($key, $row);
  }

}
