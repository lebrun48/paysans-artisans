<?php

//Rights
define('ROLE_NO_ROLE', 0);
define('ROLE_CASHIER', 1);
define('ROLE_ADMIN_LOCAL', 2);
define('ROLE_ADMIN_GLOBAL', 3);

function setRights()
{
  //Pages access
  utils()->setPageAccesRights([
      ROLE_ADMIN_LOCAL  => ["users", "cashPrint", "cashOp", "cash"],
      ROLE_CASHIER      => ["cashOp", "users", "ticket", "cash"],
      ROLE_ADMIN_GLOBAL => ["pr", "users", "cashPrint", "cashOp", "cash"],
  ]);

  //Action refused
  utils()->actionRefused = [
      "all"                                     => [
          [
              "page"   => ["cash", "ticket"],
              "action" => ["insert"],
              "type"   => ["!"] //'!' means is not present '*' is present
          ]
      ],
      utils()->getUserRolesString(ROLE_CASHIER) => [
          [
              "page"   => ["users"],
              "action" => ["*"]
          ],
          [
              "page"    => ["users"],
              "xAction" => ["setPRResponsible"]
          ]
      ],
  ];

  if (utils()->isRoot) {
    return;
  }
  //table limitation
  $roles = utils()->roles;
  //limit page content
  if ($roles[ROLE_ADMIN_LOCAL]) {
    dbUtil()->getTable("users")[DB_DEFAULT_INSERT][] = ["pr" => utils()->userSession()["pr"]];
  }
  if (utils()->isUserRole(ROLE_CASHIER)) {
    dbUtil()->getTable("users")[DB_RIGHTS_ACCES] = TABLE_RIGHT_ONLY_DISPLAY;
  }
}
