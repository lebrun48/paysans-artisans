<?php

include "common/src/buildCompos.php";
include "common/src/tableDef.php";

//Cash table defines
define('CASH_TYPE_SALE', 1);
define('CASH_TYPE_RETURN', 2);
define('CASH_TYPE_CAUTION', 3);
define('CASH_TYPE_TRANSFER', 4);
define('CASH_TYPE_ADJUST', 5);
define('CASH_TYPE_ROUNDED', 6);
define('CASH_TYPE_INIT', 7);
define('CASH_TYPE_AUTO_INIT', 8);
define('CASH_TYPE_TICKET', 9);

define('IN_OUT_MISSING', 1);
define('IN_OUT_DAMAGING', 2);
define('IN_OUT_TOO_SMALL', 3);
define('IN_OUT_TOO_MUCH', 4);
define('IN_OUT_REFUSED', 5);
define('IN_OUT_OTHER', 6);

function buildTableDefs()
{
  $users = SITE_LOGIN_TABLE;
//cash tableId
  dbUtil()->tables["cash"] = [
      DB_COLS_DEFINITION => [
          "date"        => [
              COL_GROUP       => 0,
              COL_TYPE        => "date[d F Y]",
              COL_TD_ATTR     => "colspan=3 class=group-col",
              COL_GROUP_ORDER => "desc"],
          "type"        => [COL_TITLE => "Type", COL_TD_ATTR => "class=td-1"],
          "description" => [COL_TITLE => "Description"],
          "move"        => [COL_TITLE => "Montant", COL_TYPE => "money", COL_TD_ATTR=>"class=text-nowrap"],
          "ri"          => [
              COL_HIDDEN  => true,
              COL_PRIMARY => true],
      ],
      DB_TABLENAME       => "cash",
      DB_DEFAULT_INSERT  => [
          "user"     => utils()->userSession()["ri"],
          "pr"       => utils()->userSession()["pr"],
          "isTicket" => 0,
          "date"     => utils()->now->format("Y-m-d")
      ],
      DB_DEFAULT_WHERE   => "pr=" . utils()->userSession()["pr"] . " and not isTicket",
      DB_RIGHTS_ACCES    => TABLE_RIGHT_ALL
  ];

//ticket tableId
  dbUtil()->tables["ticket"] = [
      DB_COLS_DEFINITION => [
          "type"        => [COL_TITLE => "Type", COL_TD_ATTR => "class=td-1"],
          "description" => [COL_TITLE => "Description"],
          "move"        => [COL_TITLE => "Montant", COL_TYPE => "money"],
          "ri"          => [COL_HIDDEN => true, COL_PRIMARY => true]
      ],
      DB_TABLENAME       => "cash",
      DB_DEFAULT_INSERT  => [
          "user" => utils()->userSession()["ri"],
          "pr"   => utils()->userSession()["pr"],
          "date" => utils()->now->format("Y-m-d")],
      DB_DEFAULT_WHERE   => "user=" . utils()->userSession()["ri"] . " and isTicket",
      DB_RIGHTS_ACCES    => TABLE_RIGHT_ALL
  ];

//users tableId
  dbUtil()->tables["users"] = [
      DB_TABLENAME       => $users,
      DB_COLS_DEFINITION => [
          "name"     => [
              COL_TITLE   => "Prénom NOM",
              COL_DB      => "concat(firstName, ' ', upper($users.name))",
              COL_TD_ATTR => !utils()->isUserRole(ROLE_CASHIER) ? "class=td-1" : null,
          ],
          "mail"     => [COL_TITLE => "E-Mail"],
          "phoneNbr" => [COL_TITLE => "N° tél.", COL_TD_NO_ACTION => true],
          "prName"   => [
              COL_GROUP       => 0,
              COL_TD_ATTR     => "colspan=4 class=group-col",
              COL_GROUP_ORDER => "asc",
              COL_DB          => "pr.name"
          ],
          "PRResp"   => [COL_HIDDEN => true, COL_DB => "pr.responsible"],
          "roles"    => [COL_TITLE => "Fonction(s)", COL_TD_ATTR => "class=text-nowrap"],
          "pr"       => [COL_HIDDEN => true, COL_EXT_REF => "pr"],
          "ri"       => [COL_HIDDEN => true, COL_PRIMARY => true, COL_DB => "$users.ri"]
      ],
      DB_RIGHTS_ACCES    => TABLE_RIGHT_ALL,
      DB_DEFAULT_WHERE   => "mail is not null"
  ];

//pr tableId
  dbUtil()->tables["pr"] = [
      DB_TABLENAME       => "pr",
      DB_EXTERNAL_REF    => [["users", "pr"]],
      DB_COLS_DEFINITION => [
          "name"        => [COL_TITLE => "Nom", COL_DB => "pr.name", COL_TD_ATTR => "class=td-1"],
          "cashLimit"   => [COL_TITLE => "Limite de caisse", COL_TYPE => "money-clear", COL_TH_ATTR => "class=text-right"],
          "cashContent" => [COL_TITLE => "Contenu actuel", COL_TD_ATTR => "class=text-right", COL_TH_ATTR => "class=text-right", COL_NO_DB => true],
          "responsible" => [COL_HIDDEN => true, COL_EXT_REF => "users"],
          "lastClosure" => [COL_TITLE => "Dernière clôture", COL_NO_DB => true, COL_TD_NO_ACTION => true],
          "cashSheet"   => [COL_TITLE => "Autres clôtures", COL_NO_DB => true, COL_TD_NO_ACTION => true],
          "respName"    => [COL_TITLE => "Responsable PR", COL_DB => "concat($users.firstName, ' ', upper($users.name))"],
          "ri"          => [COL_HIDDEN => true, COL_PRIMARY => true, COL_DB => "pr.ri"]
      ],
      DB_RIGHTS_ACCES    => TABLE_RIGHT_ALL
  ];

  if (utils()->isUserRole(ROLE_CASHIER)) {
    $users = &dbUtil()->tables["users"][DB_COLS_DEFINITION];
    unset($users["mail"]);
    unset($users["roles"]);
  }
}
