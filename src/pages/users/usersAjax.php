<?php

function axUsersSubmit()
{
  $_REQUEST["val_allowTrace"] || ($_REQUEST["val_allowTrace"] = "=null");
  ($phone = &$_REQUEST["val_phoneNbr"]) && ($phone = "='$phone'");

  utils()->checkUserResponse();
  switch (utils()->action) {
    case "insert":
    case "update":
      if (!utils()->setRolesCol()) {
        msgBox("<p>L'utilisateur doit avoir au moins une fonction!</p>", "Pas de fonction!", MODAL_SIZE_SMALL, [
            MSGBOX_MODAL_ATTR => [
                MODAL_NO_FADE     => true,
                MODAL_NO_BACKDROP => true,
                MODAL_CENTERED    => true
        ]]);
        exit();
      }
      break;

    case "delete":
      if (utils()->isDemo && ($row = dbUtil()->getCurrentEditedRow()) && $row["ri"] == $row["demoUser"]) {
        dbUtil()->deleteRow("users", "1");
        dbUtil()->query("delete from " . dbUtil()->getTableName("pr") . " where demoUser=" . $row["demoUser"]);
        dbUtil()->deleteRow("cash", "1");
      }
  }
  try {
    utils()->axRefreshElement("users");
  } catch (Exception $e) {
    if ($e->getCode() == E_DB_DUPLICATE) {
      msgBox([
          MSGBOX_CONTENT    => "<p>Un utilisateur avec la même adresse mail est déjà présent.</p>",
          MSGBOX_TITLE      => "Déja existant",
          MSGBOX_MODAL_ID   => "exception",
          MSGBOX_SIZE       => MODAL_SIZE_SMALL,
          MSGBOX_MODAL_ATTR => [MODAL_NO_BACKDROP => true, MODAL_CENTERED => true, MODAL_NO_FADE => true]
      ]);
      exit();
    }
    throw $e;
  }
}

function axUsersSetPRResponsible()
{
  dbUtil()->query("update pr set responsible=" . $_REQUEST["user"] . " where ri=" . $_REQUEST["pr"]);
  utils()->axExecuteJS("msgBoxClose");
}

function insertUsersTerminated()
{
  $userRi = dbUtil()->getDbCnx()->insert_id;
  if ((utils()->isRoot || utils()->roles[ROLE_ADMIN_GLOBAL] || utils()->roles[ROLE_ADMIN_LOCAL])) {
    $pr = dbUtil()->fetch_row(dbUtil()->query("select responsible, name, ri from pr where ri=" . $_REQUEST["val_pr"]));
    if (!$pr[0]) {
      msgBox("Faut-il mettre <b>" . $_REQUEST["val_firstName"] . " " . strtoupper($_REQUEST["val_name"]) . "</b> comme responsable du PR de '$pr[1]'",
                                                                                  "Responsable PR",
                                                                                  MODAL_SIZE_SMALL, [
          MSGBOX_SHOW_AFTER    => true,
          MSGBOX_BUTTON_ACTION => ["oui", "onclick=axExecute({xAction:'setPRResponsible',page:'users',pr:" . $_REQUEST["val_pr"] . ",user:$userRi})"],
          MSGBOX_BUTTON_CLOSE  => "non",
          MSGBOX_MODAL_ATTR    => [MODAL_CENTERED => true, MODAL_NO_FADE => true]
      ]);
    }
  }
}

function deleteUsersTerminated()
{
  dbUtil()->query("update pr set responsible = null where " . str_replace("ri=", "responsible=", dbUtil()->getPrimary()));
}
