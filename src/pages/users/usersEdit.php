<?php

include "common/src/BuildForm.php";

function axUsersEdit()
{
  $row = dbUtil()->getCurrentEditedRow();
  if (utils()->action == 'delete') {
    if ($row["ri"] == 0) {
      msgbox("Jack, tu peux pas supprimer 'root'!");
      exit();
    }

    $msg == msgBox([
                MSGBOX_TITLE         => "Confirmation",
                MSGBOX_CONTENT       => ($row["demoUser"] == $row["ri"] ? "<h5 style=color:rgb(245,20,20>En supprimant vous-même, vous perdez toutes les informations !</h5>" : '')
                . "<p>Voulez-vous supprimer cet utilisateur?</p>",
                MSGBOX_SIZE          => MODAL_SIZE_SMALL,
                MSGBOX_BUTTON_ACTION => "Oui",
                MSGBOX_BUTTON_CLOSE  => "Non"
                    ]
    );
    return;
  }


  //insert or update
  $roles = utils()->roles;

  $arForm = [
      ED_FORM_ATTR      => "class='px-3'",
      "firstName"       => [
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_LABEL    => "Prénom",
          ED_VALIDATE => [
              ED_VALIDATE_INVALIDE   => "Doit contenir entre 1 et 20 caractères",
              ED_VALIDATE_REQUIRED   => true,
              ED_VALIDATE_MAX_LENGTH => 20,
          ]
      ],
      "name"            => [
          ED_TYPE     => ED_TYPE_ALPHA,
          ED_LABEL    => "Nom",
          ED_VALIDATE => [
              ED_VALIDATE_INVALIDE   => "Doit contenir entre 1 et 20 caractères",
              ED_VALIDATE_REQUIRED   => true,
              ED_VALIDATE_MAX_LENGTH => 20,
          ]
      ],
      "mail"            => [
          ED_TYPE     => "email",
          ED_LABEL    => "E-Mail adresse",
          ED_VALIDATE => [
              ED_VALIDATE_INVALIDE   => "Le caractère '@' est nécessaire",
              ED_VALIDATE_MAX_LENGTH => "50",
              ED_VALIDATE_REQUIRED   => true
          ]
      ],
      "phoneNbr"        => [
          ED_TYPE     => "tel",
          ED_LABEL    => "N° téléphone",
          ED_ATTR     => "pattern='[+() 0-9]{9,20}'",
          ED_VALIDATE => [ED_VALIDATE_INVALIDE => "L'entrée ne correspond pas à un N° de tél"]
      ],
      "pr"              => [
          ED_TYPE        => ED_TYPE_SELECT,
          ED_PLACEHOLDER => "Sélectionner",
          ED_LABEL       => "Fait partie du PR",
          ED_OPTIONS     => getPRList(),
          ED_VALIDATE    => [ED_VALIDATE_REQUIRED => true, ED_VALIDATE_INVALIDE => "Utilisateur doit faire partie d'un PR"]
      ],
      [ED_INSERT_HTML => ''
          . '<div style="border:1px solid lightgrey;border-radius:4px" class="p-2">'
          . '  <h1 class=h6>Fonctions</h1>'],
      "!R_cashier"      => [
          ED_TYPE    => ED_TYPE_CHECK,
          ED_LABEL   => "Enregistrement opérations clients",
          ED_CHECKED => !$row,
          ED_VALUE   => ROLE_CASHIER
      ],
      "!R_admin1"       => [
          ED_NAME  => "!R_admin",
          ED_TYPE  => ED_TYPE_CHECK,
          ED_LABEL => "Administrateur local PR",
          ED_VALUE => ROLE_ADMIN_LOCAL
      ],
      "!R_admin"        => [
          ED_TYPE    => ED_TYPE_RADIO,
          ED_OPTIONS => [
              [ED_LABEL => "Pas Administrateur", ED_VALUE => 0, ED_CHECKED => !$row],
              [ED_LABEL => "Administrateur local PR", ED_VALUE => ROLE_ADMIN_LOCAL],
              [ED_LABEL => "Administrateur global", ED_VALUE => ROLE_ADMIN_GLOBAL],
          ]
      ],
      "!R_admin_hidden" => [
          ED_TYPE  => ED_TYPE_HIDDEN,
          ED_NAME  => "!R_admin",
          ED_VALUE => $roles[ROLE_ADMIN_GLOBAL] ? ROLE_ADMIN_GLOBAL : ROLE_ADMIN_LOCAL
      ],
      [ED_INSERT_HTML => "</div>"],
      "allowTrace"      => [ED_TYPE => ED_TYPE_CHECK, ED_LABEL => "view Trace on Prod", ED_VALUE => '1']
  ];

  if (utils()->isRoot) {
    unset($arForm["!R_admin1"]);
    unset($arForm["!R_admin_hidden"]);
  }
  else {
    unset($arForm["allowTrace"]);
    if ($roles[ROLE_ADMIN_GLOBAL]) {
      unset($arForm["!R_admin1"]);
      if (utils()->userSession()["ri"] == $row["ri"]) {
        unset($arForm["!R_admin"]);
      }
      else {
        unset($arForm["!R_admin_hidden"]);
      }
    }
    elseif ($roles[ROLE_ADMIN_LOCAL]) {
      $arForm["pr"] = [ED_TYPE => ED_TYPE_HIDDEN, ED_VALUE => utils()->userSession()["pr"]];
      unset($arForm["!R_admin"]);
      if (utils()->userSession()["ri"] == $row["ri"]) {
        unset($arForm["!R_admin1"]);
      }
      else {
        unset($arForm["!R_admin_hidden"]);
      }
    }
  }


  msgBox([
      MSGBOX_TITLE         => ($row ? "Modification" : "Création") . " utilisateur",
      //MSGBOX_SIZE          => MODAL_SIZE_SMALL,
      MSGBOX_CONTENT       => BuildForm::getForm($arForm, $row),
      MSGBOX_BUTTON_ACTION => "Enregistrer",
      MSGBOX_BUTTON_CLOSE  => "Annuler",
  ]);
}

function getPRList()
{
  $res = dbUtil()->selectRow("pr", "ri, name", '', false);
  while ($tup = dbUtil()->fetch_row($res)) {
    $options[] = [ED_VALUE => $tup[0], ED_CONTENT => $tup[1]];
  }
  return $options;
}
