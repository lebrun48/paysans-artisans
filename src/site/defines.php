<?php

include "pages/setRights.php";

//site specific
define('SITE_PROJECT', "paysans");
define('SITE_NO_DEMO', false);
//Login
define('SITE_SECURE_ONLY', true);
define('SITE_ALWAYS_LOGIN', true);
define('SITE_LOGIN_TABLE', "users_paysans");
define('SITE_LOGIN_SELECT', "select " . SITE_LOGIN_TABLE . ".*, pr.name as prName from " . SITE_LOGIN_TABLE . " left join pr on pr.ri=" . SITE_LOGIN_TABLE . ".pr");

//Colors
define('SITE_DEFAULT_TEXT_COLOR', "amber- text");
define('SITE_DEFAULT_COLOR', "amber text-white");
define('SITE_DEFAULT_BUTTON_COLOR', "btn-amber");
