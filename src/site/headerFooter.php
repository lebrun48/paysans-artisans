<?php

define("MENU_USERS", 'users');
define("MENU_PR", "pr");
define("MENU_CASH", "cash");
define("MENU_LOGOUT", "logout");

function buildHeader()
{
  echo ""
  . "  <h4 style=overflow-x:auto class='h4 text-center text-nowrap my-lg-3'>CAISSE PR de " . utils()->userSession()["prName"] . "</h4>"
  . "  <p class='my-0 text-right' style=font-size:15px class=ml-2>(" . utils()->userSession()["firstName"] . " " . strtoupper(utils()->userSession()["name"]) . ")</p>";
  buildMenu();
}

include "common/src/buildMenu.php";

function buildMenu()
{
  $menu = [
      NAV_COLOR      => strtok(SITE_DEFAULT_COLOR, " ") . " navbar-light",
      NAV_POSITION   => NAV_POSITION_STICKY_TOP,
      NAV_ATTR       => "class = py-0",
      NAV_LINK       =>
      [
          MENU_PR    => [
              NAV_LINK_ICON    => '<i class="fas fa-warehouse mr-1"></i>',
              NAV_LINK_HIDE_ON => "md",
              NAV_LINK_TITLE   => "PR",
              NAV_LINK_ATTR    => "onclick = load('pr')",
          ],
          MENU_USERS => [
              NAV_LINK_ICON    => '<i class="fas fa-user-lock mr-1"></i>',
              NAV_LINK_HIDE_ON => "md",
              NAV_LINK_TITLE   => "Utilisateurs",
              NAV_LINK_ATTR    => "onclick = load('users')",
          ],
          MENU_CASH  => [
              NAV_LINK_ICON    => '<i class="fas fa-cash-register mr-1"></i>',
              NAV_LINK_HIDE_ON => utils()->roles[ROLE_ADMIN_GLOBAL] && utils()->getCurrentPageName() == "cash" ? "md" : null,
              NAV_LINK_TITLE   => "Caisse",
              NAV_LINK_ATTR    => "onclick = load('cashOp')",
          ],
      ],
      NAV_LINK_RIGHT =>
      [
          MENU_LOGOUT => [
              NAV_LINK_TITLE   => "Déconnexion",
              NAV_LINK_HIDE_ON => "sm",
              NAV_LINK_ICON    => '<i class="fas fa-sign-out-alt mr-1"></i>',
              NAV_LINK_ATTR    => "onclick = axExecute('logout')"
          ]
      ]
  ];

  $toRemove = [];
  if (!utils()->isRoot) {
    (utils()->isUserRole(ROLE_CASHIER) || utils()->roles[ROLE_ADMIN_LOCAL]) && ($toRemove = [MENU_PR]);
    !utils()->isLocalTest && !utils()->isLogAs && !utils()->isDemo && ($toRemove = array_merge($toRemove, [MENU_LOGOUT]));
    !utils()->hasUserRole(ROLE_CASHIER) && utils()->hasUserRole(ROLE_ADMIN_GLOBAL) && ($toRemove = array_merge($toRemove, [MENU_CASH]));
  }


  buildNavbar($menu, $toRemove);
  utils()->insertReadyFunction("menuListener");
}
