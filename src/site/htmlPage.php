<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Caisse Paysans-Artisans</title>

  <!-- cpnamur icon -->
  <link rel="icon" href="https://eshop.paysans-artisans.be/paysans/static/images/favicon.ico" type="image/x-icon">
  <?php
  
  Plugin::buildIncludeCSSs();
  Plugin::staticBuildIncludeCSS(SITE_PROJECT, utils()->projectRootPath."/include/page.css", utils()->isDevMode);
  
  utils()->htmlHeaderLoaded = true;
  ?>


</head>

