<?php

include "htmlPage.php";

echo "<body class='grey lighten-5'>";
echo "<div id=debug_info></div><div id=modals></div>";

if (!utils()->hasSession()) {
  Login::get()->userLogin("Connexion caisse", "img/logo.jpg", "amber");
}
else {
  if (utils()->hasSession()) {
    echo "<div class=container>";
    include "headerFooter.php";
    if (function_exists("buildHeader")) {
      buildHeader();
    }
    include utils()->getCurrentPageName() . "Main.php";
    buildMain();

    if (function_exists("buildFooter")) {
      buildFooter();
    }
    utils()->insertIncludeScripts("page");
    echo "</div>";
  }
}

include "scriptPage.php";
echo "</body>";

