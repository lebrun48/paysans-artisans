<?php

class LoginMAil extends GenLoginMail
{

  function getValue($key)
  {
    if ($key == "prName") {
      return $this->user["prName"];
    }
    return parent::getValue($key);
  }

}

function loginMailValidation($user)
{
  $buildMail = new LoginMail($user, "validation_" . $user["ri"] . ".html", "loginValidation.html");
  $buildMail->run();

  if (!serviceConfig()->config["mail"]["test"] && utils()->isDevMode) {
    msgBox([
        MSGBOX_CONTENT    => utf8_encode(file_get_contents($buildMail->outFile)),
        MSGBOX_TITLE      => "Mail Envoyé à " . $user["name"],
        MSGBOX_SHOW_AFTER => true,
        MSGBOX_MODAL_ATTR => [MODAL_POS => MODAL_POS_LEFT, MODAL_DIR => MODAL_DIR_LEFT]
    ]);
    return;
  }

  new SendMail("Validation accès Caisse PR " . $user["prName"], $user["mail"], $buildMail->outFile);
}

function loginMailResetPwd($user)
{
  $buildMail = new LoginMail($user, "resetPwd_" . $user["ri"] . ".html", "loginResetPwd.html");
  $buildMail->run();

  if (!serviceConfig()->config["mail"]["test"] && (!utils()->isRoot || utils()->isLocalTest) && utils()->isDevMode) {
    msgBox([
        MSGBOX_CONTENT    => utf8_encode(file_get_contents($buildMail->outFile)),
        MSGBOX_TITLE      => "Mail Envoyé à " . $user["name"],
        MSGBOX_SHOW_AFTER => true,
        MSGBOX_MODAL_ATTR => [MODAL_POS => MODAL_POS_LEFT, MODAL_DIR => MODAL_DIR_LEFT]
    ]);
    return;
  }

  new SendMail("Nouveau mot de passe Caisse PR " . $user["prName"], $user["mail"], $buildMail->outFile);
}

function setUserDemoDBEnv($userId)
{
  dbUtil()->query("insert into pr set name='Démo', cashLimit=100, responsible=$userId, demoUser=$userId");
  return "pr=" . dbUtil()->getDbCnx()->insert_id . ", firstName='Premier', name='Admin', roles="
          . ($_REQUEST["demoData"] == "client" ? "'R1'" : ($_REQUEST["demoData"] == "local" ? "'R11'" : "'R101'"));
}

function getUserDemoMsg()
{
  return ""
          . "<p>Bienvenue sur la version 'démo' de la caisse des points de R'Aliment de paysans-artisans!</p>"
          . "<p>Si vous voulez tester cette version cliquez sur 'OK', vous "
          . ($_REQUEST["demoData"] == "client" ? "pourrez alors tester l'encodage des opérations clients." : "deviendrez alors "
          . ($_REQUEST["demoData"] == "local" ? "Administrateur d'un point de R'Aliment." : "Administrateur du système."))
          . "</p><h6 class=h6>Bon amusement!</h6>";
}

function getUserDemoTitle()
{
  return "Démo caisses PR";
}
